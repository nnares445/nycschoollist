import Foundation
import UIKit

//MARK: - Extensions
extension UIViewController {
    public func showAlertMessage(title:String = "", message:String, cancelButtonTitle:String = "OK"){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: cancelButtonTitle, style: .cancel, handler: nil))
        alert.view.tintColor = .magenta
        self.present(alert, animated: true)
    }
}
