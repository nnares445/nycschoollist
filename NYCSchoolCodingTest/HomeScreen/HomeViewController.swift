import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var searchBar: UISearchBar?
    @IBOutlet weak var usersTableView: UITableView?
    
    static let userCell = "userCell"
    // Final School List.
    var schoolList: [SchoolListModel] = []
    // Searched School List.
    var searchList: [SchoolListModel] = []
    /// To check for reachability
    var reachability: Reachability!
    /// To store data in userdefaults.
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "School list screen"
        setupSearchBarField()
        getDataFromJSON()
        setupRefreshControl()
    }

    func setupSearchBarField() {
        searchBar?.text = ""
        searchBar?.delegate = self
        searchBar?.returnKeyType = .done
    }

    func checkReachability() {
        do {
            try reachability = Reachability()
            NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged(_:)), name: Notification.Name.reachabilityChanged, object: reachability)
            try reachability.startNotifier()
        } catch {
            self.showAlertMessage(title: "Alert", message: "Reachability is not working", cancelButtonTitle: "Cancel")
        }
    }
    
    @objc func reachabilityChanged(_ note: NSNotification) {
        let reachability = note.object as! Reachability
        if reachability.connection == .unavailable {
            self.showAlertMessage(title: "Alert", message: "Network unavailable", cancelButtonTitle: "Cancel")
        }
    }

    func setupRefreshControl() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(didPullToRefresh(_:)), for: .valueChanged)
        usersTableView?.addSubview(refreshControl)
        usersTableView?.sendSubviewToBack(refreshControl)
        usersTableView?.alwaysBounceVertical = true
    }
    
    @objc func didPullToRefresh(_ refreshControl: UIRefreshControl) {
        // Handle here for pull to refresh action.
        self.getDataFromJSON()
        self.view.endEditing(true)
        self.searchBar?.text = ""
        refreshControl.endRefreshing()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.searchBar?.endEditing(true)
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar?.endEditing(true)
    }
}

extension HomeViewController {
    func getDataFromJSON() {
        checkReachability()
        if let url = URL(string: "https://data.cityofnewyork.us/resource/734v-jeq5") {
           URLSession.shared.dataTask(with: url) { data, response, error in
              if let data = data {
                  do {
                    let res = try JSONDecoder().decode(Array<SchoolListModel>.self, from: data)
                    self.defaults.set("\(res)", forKey: "SchoolList")
                    self.schoolList = res
                    self.searchList = res
                    DispatchQueue.main.async {
                        self.usersTableView?.reloadData()
                    }
                  } catch let error {
                    self.showAlertMessage(title: "Alert", message: error.localizedDescription, cancelButtonTitle: "Cancel")
                  }
              } else {
                if let value = self.defaults.value(forKey: "SchoolList") as? [SchoolListModel] {
                    self.searchList = value
                    self.schoolList = value
                }
            }
           }.resume()
        }
    }
}

extension HomeViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(userPausedTyping(searchText:)), object: searchText)
        self.perform(#selector(userPausedTyping(searchText:)), with: searchText, afterDelay: 0.25)
    }
    
    @objc func userPausedTyping(searchText: String) {
        if searchText.isEmpty {
            searchList = schoolList
            usersTableView?.reloadData()
            return
        }
        searchList.removeAll()
        for value in schoolList where value.school_name?.lowercased().contains(searchText.lowercased()) ?? false {
            self.searchList.append(value)
        }
        usersTableView?.reloadData()
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HomeViewController.userCell) as? ListTableViewCell else {
            return UITableViewCell(style: .default, reuseIdentifier: HomeViewController.userCell)
        }
        if schoolList.count > indexPath.row {
            let viewModel = ListTableViewCellViewModel(list: searchList[indexPath.row])
            cell.viewModel = viewModel
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let detailVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailsViewControllerID") as? DetailsViewController else { return }
        let viewModel = DetailsViewControllerViewModel(list: searchList[indexPath.row])
        detailVC.viewModel = viewModel
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
}
