import UIKit

public class ListTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel?

    var viewModel: ListTableViewCellViewModel? {
        didSet {
            updateUI()
        }
    }

    func updateUI() {
        guard let userData = viewModel?.list else { return }
        nameLabel?.text = userData.school_name
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
    }

    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
