import Foundation
import UIKit

public struct ListTableViewCellViewModel {
    var list: SchoolListModel?

    init(list: SchoolListModel? = nil) {
        self.list = list
    }
}
