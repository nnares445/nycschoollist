import UIKit

class DetailsViewController: UIViewController {
    @IBOutlet weak var schoolNameLabel: UILabel?
    @IBOutlet weak var dbnLabel: UILabel?
    @IBOutlet weak var satTestLabel: UILabel?
    @IBOutlet weak var readingAvgScoreLabel: UILabel?
    @IBOutlet weak var mathAvgScoreLabel: UILabel?
    @IBOutlet weak var writingAvgScoreLabel: UILabel?

    var viewModel = DetailsViewControllerViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "School Details Screen"
        updateUI()
    }
    
    func updateUI() {
        guard let userData = viewModel.list else {
            return
        }
        DispatchQueue.main.async { [weak self] in
            if let name = userData.school_name {
                self?.schoolNameLabel?.text = "Name: \(name)"
            }
            if let dbn = userData.dbn {
                self?.dbnLabel?.text = "DBN: \(dbn)"
            }
            if let satTest = userData.num_of_sat_test_takers {
                self?.satTestLabel?.text = "SAT test takers: \(satTest)"
            }
            if let score = userData.sat_critical_reading_avg_score {
                self?.readingAvgScoreLabel?.text = "Average CR score: \(score)"
            }
            if let avgScore = userData.sat_math_avg_score {
                self?.mathAvgScoreLabel?.text = "Average Math score: \(avgScore)"
            }
            if let writingScore = userData.sat_writing_avg_score {
                self?.writingAvgScoreLabel?.text = "Average Writing score: \(writingScore)"
            }
        }
    }
}
