import Foundation
import UIKit

/// DetailsViewControllerViewModel
public struct DetailsViewControllerViewModel {
    var list: SchoolListModel?

    init(list: SchoolListModel? = nil) {
        self.list = list
    }
}
