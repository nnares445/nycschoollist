import XCTest
@testable import NYCSchoolCodingTest

class ListTableViewCellViewModelTests: XCTestCase {

    var viewModel: ListTableViewCellViewModel!
    var list: SchoolListModel!

    override func setUp() {
        super.setUp()
        list = SchoolListModel(dbn: "01M292", school_name: "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES", num_of_sat_test_takers: "29", sat_critical_reading_avg_score: "355", sat_math_avg_score: "404", sat_writing_avg_score: "363")
        viewModel = ListTableViewCellViewModel(list: list)
    }

    override func tearDown() {
        viewModel = nil
        list = nil
        super.tearDown()
    }

    func testDbn() {
        let dbn = viewModel.list?.dbn
        XCTAssertEqual(dbn, "01M292")
    }
    
    func testSchoolName() {
        let schoolName = viewModel.list?.school_name
        XCTAssertEqual(schoolName, "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES")
    }
    
    func testSatTestTakers() {
        let satTest = viewModel.list?.num_of_sat_test_takers
        XCTAssertEqual(satTest, "29")
    }
    
    func testCR() {
        let cr = viewModel.list?.sat_critical_reading_avg_score
        XCTAssertEqual(cr, "355")
    }
    
    func testMathAvg() {
        let avg = viewModel.list?.sat_math_avg_score
        XCTAssertEqual(avg, "404")
    }
    
    func testWritingAvg() {
        let avg = viewModel.list?.sat_writing_avg_score
        XCTAssertEqual(avg, "363")
    }
}
