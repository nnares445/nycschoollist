import XCTest
@testable import NYCSchoolCodingTest

class DetailsViewControllerTests: XCTestCase {
    var viewModel: DetailsViewControllerViewModel!
    var list: SchoolListModel!
    var viewController: DetailsViewController!
    
    override func setUp() {
        super.setUp()
        list = SchoolListModel(dbn: "01M292", school_name: "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES", num_of_sat_test_takers: "29", sat_critical_reading_avg_score: "355", sat_math_avg_score: "404", sat_writing_avg_score: "363")
        viewModel = DetailsViewControllerViewModel(list: list)
        viewController = DetailsViewController()
        viewController.viewModel = viewModel
    }

    override func tearDown() {
        viewModel = nil
        list = nil
        viewController = nil
        super.tearDown()
    }
    
    func testTitle() {
        let title = viewController.title
        XCTAssertEqual(title, nil)
    }

    func testDbn() {
        let dbn = viewController.viewModel.list?.dbn
        XCTAssertEqual(dbn, "01M292")
    }
    
    func testSchoolName() {
        let schoolName = viewController.viewModel.list?.school_name
        XCTAssertEqual(schoolName, "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES")
    }
    
    func testSatTestTakers() {
        let satTest = viewController.viewModel.list?.num_of_sat_test_takers
        XCTAssertEqual(satTest, "29")
    }
    
    func testCR() {
        let cr = viewController.viewModel.list?.sat_critical_reading_avg_score
        XCTAssertEqual(cr, "355")
    }
    
    func testMathAvg() {
        let avg = viewController.viewModel.list?.sat_math_avg_score
        XCTAssertEqual(avg, "404")
    }
    
    func testWritingAvg() {
        let avg = viewController.viewModel.list?.sat_writing_avg_score
        XCTAssertEqual(avg, "363")
    }
}
