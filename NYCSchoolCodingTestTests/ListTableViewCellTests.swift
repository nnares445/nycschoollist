import XCTest
@testable import NYCSchoolCodingTest

class ListTableViewCellTests: XCTestCase {

    var tableViewCell: ListTableViewCell!

    override func setUp() {
        super.setUp()
        tableViewCell = ListTableViewCell()
    }

    override func tearDown() {
        tableViewCell = nil
        super.tearDown()
    }

    func testDbn() {
        let name = tableViewCell.nameLabel?.text
        XCTAssertEqual(name, nil)
    }
}
