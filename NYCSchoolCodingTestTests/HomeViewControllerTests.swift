//
//  HomeViewControllerTests.swift
//  NYCSchoolCodingTestTests
//
//  Created by Naresh Nadhendla on 5/8/20.
//  Copyright © 2020 Naresh Nadhendla. All rights reserved.
//

import XCTest
@testable import NYCSchoolCodingTest

class HomeViewControllerTests: XCTestCase {
    var viewController: HomeViewController!

    override func setUp() {
        super.setUp()
        viewController = HomeViewController()
    }

    override func tearDown() {
        viewController = nil
        super.tearDown()
    }

    func testListCount() {
        let title = viewController.schoolList.count
        XCTAssertEqual(title, 0)
    }

    func testTitle() {
        let title = viewController.title
        XCTAssertEqual(title, nil)
    }

    func testSearchListCount() {
        let title = viewController.searchList.count
        XCTAssertEqual(title, 0)
    }

    func testSearchBarText() {
        let title = viewController.searchBar?.text
        XCTAssertEqual(title, nil)
    }
}
